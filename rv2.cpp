#include <iostream>
using namespace std;


// IZRACUN MEDIANE

float izracunajMediano(float vrednost, int kolicina) {
	float rezultat = vrednost / kolicina;
	return rezultat;
}


// SPREMENI

float convert(float faktor1, float faktor2) {
	return faktor1 * (faktor2/100);
}


// KRITERIJI ZA OCENE

int dolociOceno(float odstotki) {
	int ocena;
	if(odstotki < 60 && odstotki >= 50) {
		ocena = 6;
	} else if(odstotki < 70 && odstotki >= 60) {
		ocena = 7;
	} else if(odstotki < 80 && odstotki >= 70) {
		ocena = 8;
	} else if(odstotki < 90 && odstotki >= 80) {
		ocena = 9;
	} else if(odstotki <= 100 && odstotki >= 90) {
		ocena = 10;
	} else if(odstotki < 50 && odstotki >= 40) {
		ocena = 4;
	} else if(odstotki < 40 && odstotki >= 30) {
		ocena = 3;
	} else if(odstotki < 30 && odstotki >= 20) {
		ocena = 2;
	} else {
		ocena = 1;
	}
	return ocena;
}


// PREGLED PRISOTNOSTI

bool imelPrisotnost(float prisotnost) {
	if(prisotnost >= 80) {
		return true;
	} else {
		return false;
	}
}


// OPRAVLJENE VAJE

bool opravilVaje(float vaje) {
	if(vaje >= 50.0) {
		return true;
	} else {
		return false;
	}
}


// VMESNI IZPITI

bool naredilIzpit(float izpit) {
	if(izpit >= 50.0) {
		return true;
	} else {
		return false;
	}
}

// POGOJI ZA OPRAVLJENE VAJE

bool izpolnjujePogoje(float prisotnost, float vaje) {
	if(imelPrisotnost(prisotnost) == true && opravilVaje(vaje) == true){
		return true;
	} else {
		return false;
	}
}

int main(){

	// DEFINIRANJE SPREMENLJIVK

	float domaceNaloge;
	float laboratorijskeVaje;
	float prisotnost;
	float kvizi;
	float vmesniIzpit1;
	float vmesniIzpit2;
	float vmesniIzpit3;
	float pisniIzpit;
	float skupneTocke;

	// STEVILO IZPITNIH ROKOV
	int izpitniRok = 3;

	// PRISOTNOST
	cout << "Na koliko odstotkih vaj je student bil prisoten? (0 - 100 odstotkov)" << endl;
	cin >> prisotnost;
	if(cin.fail()) {
		cout << "Niste vpisali stevila." << endl;
		return 0;
	} else {
		while(prisotnost > 100 || prisotnost < 0) {
			cout << "Vnesete lahko stevila od 0 do 100." << endl;
			cin >> prisotnost;
		}
	}

	// CHECK CE JE STUDENT BIL DOVOLJ PRISOTEN
	if(imelPrisotnost(prisotnost) == false) {
		cout << "STUDENT NI IMEL DOVOLJSNJE PRISOTNOSTI." << endl;
		return 0;
	}

	// LABORATORIJSKE VAJE
	cout << "Koliko odstotkov je student dosegel z laboratorijskih vaj? (0 - 100 odstotkov)" << endl;
	cin >> laboratorijskeVaje;
	if(cin.fail()) {
		cout << "Niste vpisali stevila." << endl;
	} else {
		while(laboratorijskeVaje > 100 || laboratorijskeVaje < 0) {
			cout << "Vnesete lahko stevila od 0 do 100." << endl;
			cin >> laboratorijskeVaje;
		}
	}

	// CHECK CE JE STUDENT OPRAVIL VAJE
	if(opravilVaje(laboratorijskeVaje) == false) {
		cout << "STUDENT NI OPRAVIL VAJ." << endl;
		return 0;
	}

	// DOMACE NALOGE
	cout << "Koliko odstotkov je student dosegel z domacih nalog? (0 - 100 odstotkov)" << endl;
	cin >> domaceNaloge;
	if(cin.fail()) {
		cout << "Niste vpisali stevila." << endl;
		return 0;
	} else {
		while(domaceNaloge > 100 || domaceNaloge < 0) {
			cout << "Vnesete lahko stevila od 0 do 100." << endl;
			cin >> domaceNaloge;
		}
	}

	// KVIZI
	cout << "Koliko odstotkov je student dosegel s kvizov? (0 - 100 odstotkov)" << endl;
	cin >> kvizi;
	if(cin.fail()) {
		cout << "Niste vpisali stevila." << endl;
		return 0;
	} else {
		while(kvizi > 100 || kvizi < 0) {
			cout << "Vnesete lahko stevila od 0 do 100." << endl;
			cin >> kvizi;
		}
	}

	// VMESNI IZPIT 1
	cout << "Koliko odstotkov je student dosegel z vmesnega izpita 1? (0 - 100 odstotkov)" << endl;
	cin >> vmesniIzpit1;
	if(cin.fail()) {
		cout << "Niste vpisali stevila." << endl;
		return 0;
	} else {
		while(vmesniIzpit1 > 100 || vmesniIzpit1 < 0) {
			cout << "Vnesete lahko stevila od 0 do 100." << endl;
			cin >> vmesniIzpit1;
		}
	}

	// VMESNI IZPIT 2
	cout << "Koliko odstotkov je student dosegel z vmesnega izpita 2? (0 - 100 odstotkov)" << endl;
	cin >> vmesniIzpit2;
	if(cin.fail()) {
		cout << "Niste vpisali stevila." << endl;
		return 0;
	} else {
		while(vmesniIzpit2 > 100 || vmesniIzpit2 < 0) {
			cout << "Vnesete lahko stevila od 0 do 100." << endl;
			cin >> vmesniIzpit2;
		}
	}

	// VMESNI IZPIT 3
	cout << "Koliko odstotkov je student dosegel z vmesnega izpita 3? (0 - 100 odstotkov)" << endl;
	cin >> vmesniIzpit3;
	if(cin.fail()) {
		cout << "Niste vpisali stevila." << endl;
		return 0;
	} else {
		while(vmesniIzpit3 > 100 || vmesniIzpit3 < 0) {
			cout << "Vnesete lahko stevila od 0 do 100." << endl;
			cin >> vmesniIzpit3;
		}
	}

	pisniIzpit = izracunajMediano((vmesniIzpit1 + vmesniIzpit2 + vmesniIzpit3), 3);

	skupneTocke = convert(domaceNaloge, 15) + convert(laboratorijskeVaje, 25) + convert(kvizi, 10) + convert(pisniIzpit, 50);

	if(naredilIzpit(pisniIzpit) == true) {
		cout << "------------------------" << endl
		<< "Domace naloge: " << convert(domaceNaloge, 15) << endl
		<< "Laboratorijske vaje: " << convert(laboratorijskeVaje, 25) << endl
		<< "Kvizi: " << convert(kvizi, 10) << endl
		<< "Pisni izpit: " << convert(pisniIzpit, 50) << endl
	 	<< "------------------------" << endl
		<< "Skupne tocke: " << skupneTocke << endl
		<< "OCENA: " << dolociOceno(skupneTocke) << endl
		<< "------------------------" << endl;
	} else {
		for(int i=1; i <= izpitniRok; i++) {
			cout << "Koliko odstotkov je student dosegel na " << i << ". izpitnem roku?" << endl;
			cin >> pisniIzpit;
			if(pisniIzpit >= 50.0) {
				izpitniRok = i;
			}
		}
		if(naredilIzpit(pisniIzpit)) {
			cout << "------------------------" << endl
			<< "Domace naloge: " << convert(domaceNaloge, 15) << endl
			<< "Laboratorijske vaje: " << convert(laboratorijskeVaje, 25) << endl
			<< "Kvizi: " << convert(kvizi, 10) << endl
			<< "Pisni izpit: " << convert(pisniIzpit, 50) << endl
		 	<< "------------------------" << endl
			<< "Skupne tocke: " << skupneTocke << endl
			<< "OCENA: " << dolociOceno(skupneTocke) << endl
			<< "------------------------" << endl;
		} else {
			cout << "STUDENT NI OPRAVIL PISNEGA IZPITA." << endl;
		}
	}

	return 0;
}