#include <iostream>
using namespace std;

float izracunajPloscinoTrikotnika(float stranica, float visina) {
	return ((stranica * visina) / 2);
}

float izracunajObsegTrikotnika(float a, float b, float c) {
	return (a + b + c);
}

float izracunajPloscinoKvadrata(float stranica) {
	return (stranica * stranica);
}

float izracunajObsegKvadrata(float stranica) {
	return (stranica * 4);
}

float izracunajPloscinoKroga(float polmer) {
	const double pi = 3.1415926535897;
	return (polmer * pi);
}

float izracunajObsegKroga(float polmer) {
	const double pi = 3.1415926535897;
	return (2 * pi * polmer);
}

int main(){
	float a;
	float b;
	float c;
	float d;
	int type;

	cout << "1: trikotnik, 2: kvadrat, 3: krog?" << endl;
	cin >> type;
	if(cin.fail()){
			cout << "Niste vstavili stevila." << endl;
			return 0;
		}
	if(type == 1) {
		cout << "Vstavite dolzino stranice a:" << endl;
		cin >> a;
		if(cin.fail()){
			cout << "Niste vstavili stevila." << endl;
			return 0;
		}
		cout << "Vstavite dolzino stranice b:" << endl;
		cin >> b;
		if(cin.fail()){
			cout << "Niste vstavili stevila." << endl;
			return 0;
		}
		cout << "Vstavite dolzino stranice c:" << endl;
		cin >> c;
		if(cin.fail()){
			cout << "Niste vstavili stevila." << endl;
			return 0;
		}
		cout << "Vstavite visino na stranico a:" << endl;
		cin >> d;
		if(cin.fail()){
			cout << "Niste vstavili stevila." << endl;
			return 0;
		}
		if((a + b) > c && (a + c) > b && (b + c) > a && a > 0.0 && b > 0.0 && c > 0.0) {
			cout << "Ploscina trikotnika: " << izracunajPloscinoTrikotnika(a, d) << endl;
			cout << "Obseg trikotnika: " << izracunajObsegTrikotnika(a, b, c) << endl;
		} else {
			cout << "Trikotnik ne obstaja." << endl;
			return 0;
		}
	} else if (type == 2) {
		cout << "Vstavite dolžino stranice a:" << endl;
		cin >> a;
		if(cin.fail()){
			cout << "Niste vstavili stevila." << endl;
			return 0;
		}
		if(a > 0.0) {
			cout << "Ploscina kvadrata: " << izracunajPloscinoKvadrata(a) << endl;
			cout << "Obseg kvadrata: " << izracunajObsegKvadrata(a) << endl;
		} else {
			cout << "Kvadrat ne obstaja." << endl;
			return 0;
		}
	} else if(type == 3) {
		cout << "Vstavite dolžino polmera:" << endl;
		cin >> a;
		if(cin.fail()){
			cout << "Niste vstavili stevila." << endl;
			return 0;
		}
		if(a > 0.0) {
			cout << "Ploscina kroga: " << izracunajPloscinoKroga(a) << endl;
			cout << "Obseg kroga: " << izracunajObsegKroga(a) << endl;
		} else {
			cout << "Krog ne obstaja." << endl;
		}
	}
	return 0;
}