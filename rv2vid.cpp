#include <iostream>
using namespace std;

int main()
{
    float prisotnost;
    float kvizi;
    float naloge;
    float vaje;
    float vmesniIzpit1;
    float vmesniIzpit2;
    float vmesniIzpit3;
    float izpit;
    float rezultat;
    int izpitniRok = 3;

    //Začetek
    cout << "Program za peverjanje kočne ocene pri Programiranje 1." << endl;
    cout << "--------------------------------------------------------" << endl;

    //Kvizi
    cout << "Preverjanje kvizov." << endl;
    cout << "Vstavi procente ki si jih dosegel: (0-100)." << endl;
    cin >> kvizi;
    //Za črke
    if (cin.fail()) return 0;

    if (kvizi >=0 && kvizi <=100)
    {
        cout << "--------------------------------------------------------" << endl;
        cout << "Dosegel si:" << (kvizi*10)/100 << "% od možnih 10%." << endl;
        cout << "--------------------------------------------------------" << endl;
    }
    else
    {
        cout << "--------------------------------------------------------" << endl;
        cout << "Vstavil si narobno število." << endl;
        cout << "Prosim ponovi." << endl;
        return 0;
    }

    //Domače naloge
    cout << "Preverjanje domačih nalog." << endl;
    cout << "Vstavi procente ki si jih dosegel: (0-100)." << endl;
    cin >> naloge;
    //Za črke
    if (cin.fail()) return 0;

    if (naloge >=0 && naloge <=100)
    {
        cout << "--------------------------------------------------------" << endl;
        cout << "Opravil si." << endl;
        cout << "Dosegel si:" << (naloge*15)/100 << "% od možnih 15%." << endl;
        cout << "--------------------------------------------------------" << endl;
    }
    else
    {
        cout << "--------------------------------------------------------" << endl;
        cout << "Vstavili ste narobno število." << endl;
        cout << "Ponovo zaženite program." << endl;
        return 0;
    }

    //Prisotnost
    cout << "Preverjanje prisotnosti." << endl;
    cout << "Vstavi procente, ki si jih dosegel: (0-100)." << endl;
    cin >> prisotnost;

    if (prisotnost >=80 && prisotnost <=100)
    {
        cout << "--------------------------------------------------------" << endl;
        cout << "Opravil si." << endl;
        cout << "--------------------------------------------------------" << endl;
    }
    else if (prisotnost >=0 && prisotnost <80)
    {
        cout << "--------------------------------------------------------" << endl;
        cout << "Nisi opravil." << endl;
        cout << "Ne moreš nadaljevati dokler ne izpolnjuješ vseh pogojev." << endl;
        return 0;
    }
    else
    {
        cout << "--------------------------------------------------------" << endl;
        cout << "Vstavili ste narobno število." << endl;
        cout << "Ponovo zaženite program." << endl;
        return 0;
    }

    //Vaje
    cout << "Preverjanje vaj." << endl;
    cout << "Vstavi procente, ki si jih dosegel: (0-100)." << endl;
    cin >> vaje;
    //Za črke
    if (cin.fail()) return 0;

    if (vaje >=50 && vaje <=100)
    {
        cout << "--------------------------------------------------------" << endl;
        cout << "Opravil si." << endl;
        cout << "Dosegel si:" << (vaje*25)/100 << "% od možnih 25%." << endl;
        cout << "--------------------------------------------------------" << endl;
    }
    else if (vaje >=0 && vaje <50)
    {
        cout << "--------------------------------------------------------" << endl;
        cout << "Nisi opravil." << endl;
        cout << "Ne moreš nadaljevati dokler ne izpolnjuješ vseh pogojev." << endl;
        return 0;
    }
    else
    {
        cout << "--------------------------------------------------------" << endl;
        cout << "Vstavili ste narobno število." << endl;
        cout << "Ponovo zaženite program." << endl;
        return 0;
    }

    // kolokviji
    cout << "Prvi kolokvij." << endl;
    cout << "Vstavi procente, ki si jih dosegel pri prvem kolokviju." << endl;
    cin >> vmesniIzpit1;

    cout << "Drugi kolokvij." << endl;
    cout << "Vstavi procente, ki si jih dosegel pri drugem kolokviju." << endl;
    cin >> vmesniIzpit2;

    cout << "Tretji kolokvij." << endl;
    cout << "Vstavi procente, ki si jih dosegel pri tretjem kolokviju." << endl;
    cin >> vmesniIzpit3;

    kvizi = kvizi * 0.1;
    naloge = naloge * 0.15;
    vaje = vaje * 0.25;
    izpit = ((vmesniIzpit1 + vmesniIzpit2 + vmesniIzpit3)/3)*0.5;

    rezultat = kvizi + naloge + vaje + izpit;

    if (izpit < 25) {
        for(int i=1; i <= izpitniRok; i++) {
            cout << "Koliko tock si pisal v " << i << ". izpitnem roku?" << endl;
            cin >> izpit;
            izpit = izpit * 0.5;
            if (izpit >= 25) {
                izpitniRok = i;
            }
        }
    }

    if (rezultat >=50 && rezultat <=59.9)
    {
        cout << "Tvoja končna ocena je 6." << endl;
    }
    else if (rezultat >=60 && rezultat <=69.9)
    {
        cout << "Tvoja končna ocena je 7." << endl;
    }
    else if (rezultat >=70 && rezultat <=79.9)
    {
        cout << "Tvoja končna ocena je 8." << endl;
    }
    else if (rezultat >=80 && rezultat <=89.9)
    {
        cout << "Tvoja končna ocena je 9." << endl;
    }
    else if (rezultat >=90 && rezultat <=100)
    {
        cout << "Tvoja končna ocena je 10." << endl;
    }
return 0;
}
