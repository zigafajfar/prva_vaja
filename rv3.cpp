#include <iostream>

using namespace std;

int gcd(int stevec, int imenovalec) {
	int i;
	while(stevec != 0) {
		i = stevec;
		stevec = imenovalec%stevec;
		imenovalec = i;
	}
	return i;
}

int izracunaj(int stevec1, int imenovalec1, int stevec2, int imenovalec2, char matOperator) {
	int stevec;
	int imenovalec;

	if(imenovalec1 == 0 || imenovalec2 == 0) {
		cout << "Ulomek ne obstaja." << endl;
		return 0;
	}

	if(matOperator == '+') {
		if(imenovalec1 != imenovalec2) {
			stevec = (stevec1 * imenovalec2 + imenovalec1 * stevec2);
			imenovalec = (imenovalec1 * imenovalec2);
		}	else {
			stevec = stevec1 + stevec2;
			imenovalec = imenovalec1;
		}
	} else if(matOperator == '-') {
		if(imenovalec1 != imenovalec2) {
			stevec = (stevec1 * imenovalec2 - imenovalec1 * stevec2);
			imenovalec = (imenovalec1 * imenovalec2);
		}	else {
			stevec = stevec1 - stevec2;
			imenovalec = imenovalec1;
		}
	} else if(matOperator == '*') {
		stevec = (stevec1 * stevec2);
		imenovalec = (imenovalec1 * imenovalec2);
	}	else {
		stevec = (stevec1 * imenovalec2);
		imenovalec = (stevec2 * imenovalec1);	
	}

	int x;
	x = stevec;
	stevec = stevec / gcd(stevec, imenovalec);
	imenovalec = imenovalec / gcd(x, imenovalec);

	if(imenovalec != 0) {
		cout << "--------" << endl;
		cout << stevec << "/" << imenovalec << endl;
		cout << "--------" << endl;
	} else {
		cout << "Ulomek ne obstaja." << endl;
	}

	return 0;
}

int main() {
	int stevec1;
	int imenovalec1;
	int stevec2;
	int imenovalec2;
	int counter;
	char matOperator;

	cout << "Vstavi stevec in imenovalec prvega ulomka:" << endl;

	cin >> stevec1;
	if(cin.fail()) {
		cout << "Niste vstavili pravilnega znaka." << endl;
		return 0;
	}
	cin >> imenovalec1;
	if(cin.fail()) {
		cout << "Niste vstavili pravilnega znaka." << endl;
		return 0;
	}
	cout << "Vstavi stevec in imenovalec drugega ulomka:" << endl;
	cin >> stevec2;
	if(cin.fail()) {
		cout << "Niste vstavili pravilnega znaka." << endl;
		return 0;
	}
	cin >> imenovalec2;
	if(cin.fail()) {
		cout << "Niste vstavili pravilnega znaka." << endl;
		return 0;
	}
	cout << "Vstavi operator (+, -, *, /)" << endl;
	cin >> matOperator;

	cout << imenovalec1%stevec1 << endl;
	cout << izracunaj(stevec1, imenovalec1, stevec2, imenovalec2, matOperator) << endl;
	return 0;
}