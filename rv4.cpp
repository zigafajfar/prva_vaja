#include <iostream>
#include <math.h>
#include <string>
using namespace std;

bool sodo(int x) {
	if(x % 2 == 0) {
		return true;
	}
	else {
		return false;
	}
}

float abs(float x) {
	if(x < 0) {
		return (x * (-1));
	} else {
		return x;
	}
}

double prostornina3StranePiramide(int a, int v) {
	if(a > 0 && v > 0) {
		return (((a*a) * sqrt(3) / 4) * v) / 3;
	}
}

bool prastevilo(int x) {
	if(x < 2) {
		return false;
	}
	if(x % 2 == 0) {
		return false;
	}
	for(int i = 2; i < x; i++) {
		if(x % i != 0) {
			return true;
		}
	}
	return false;
}

int fakulteta(int x) {
	if(x == 1) {
		return 1;
	} else {
		return x * fakulteta(x - 1);
	}
}

string obrniString(string &str) {
	return string(str.rbegin(), str.rend());
}

string izpisiDvojisko(int x) {
	string dvojiskiZapis;
	while(x > 0) {
		if(x % 2 == 0) {
			dvojiskiZapis += "0";
		} else {
			dvojiskiZapis += "1";
		}
		x = x/2;
	}
	dvojiskiZapis = obrniString(dvojiskiZapis);
	return dvojiskiZapis;
}

int prastevila(int x, int y) {
	int i;
	while(x <= y) {
		if(prastevilo(x) == true) {
			i++;
		}
		x++;
	}
	return i;
}

int izrisiSahovnico() {
	int i = 8;
	int j = 8;
	int k = 64;
	while(j > 0) {
		while(i > 0) {
			if(j % 2 == 0) {
				if(i % 2 == 0) {
					cout << "X ";
				} else {
					cout << "O ";
				}
				i--;
				k--;
			} else {
				if(i % 2 == 0) {
					cout << "O ";
				} else {
					cout << "X ";
				}
				i--;
				k--;
			}
		}
		cout << endl;
		j--;
		i = 8;
	}
}

int main() {

}